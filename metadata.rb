name             'selenium_grid'
maintainer       'mindera'
maintainer_email 'joao.cravo@mindera.pt'
license          'Apache 2.0'
description      'Installs selenium grid, browsers and drivers'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.2.1'

depends          'java', '~> 1.31.0'
depends          'yum'
depends          'logrotate', '~> 1.9.0'
