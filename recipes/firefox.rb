#
# Cookbook Name:: selenium_grid
# Recipe:: selenium_grid::firefox
#
# Copyright 2015, Mindera
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
FIREFOX_VERSION = node['selenium_grid']['firefox']['version']

# Installs Firefox with binary
yum_package 'firefox' do
  version FIREFOX_VERSION
  arch node['selenium_grid']['firefox']['arch'] unless node['selenium_grid']['firefox']['arch'].nil?
  only_if { node['selenium_grid']['firefox']['from_binary'] }
end

# If from source
yum_package 'bzip2'

yum_package 'firefox' do
  action :remove
  not_if { node['selenium_grid']['firefox']['from_binary'] }
end

remote_file "/usr/local/firefox-#{FIREFOX_VERSION}.tar.bz2" do
  source "http://ftp.mozilla.org/pub/mozilla.org/firefox/releases/#{FIREFOX_VERSION}/linux-x86_64/en-US/firefox-#{FIREFOX_VERSION}.tar.bz2"
  action :create
  not_if { node['selenium_grid']['firefox']['from_binary'] }
end

execute 'Untar firefox' do
  command "tar xvjf firefox-#{FIREFOX_VERSION}.tar.bz2"
  cwd '/usr/local/'
  not_if { node['selenium_grid']['firefox']['from_binary'] }
end

link '/usr/bin/firefox' do
  to '/usr/local/firefox/firefox'
  link_type :symbolic
  not_if { node['selenium_grid']['firefox']['from_binary'] }
end

# Remove tar.bz2
file "/usr/local/firefox-#{FIREFOX_VERSION}.tar.bz2" do
  action :delete
end
