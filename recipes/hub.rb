#
# Cookbook Name:: selenium_grid
# Recipe:: selenium_grid::hub
#
# Copyright 2015, Mindera
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

SELENINUM_GRID_USER = node['selenium_grid']['user']

# Stop the service if exists
service 'selenium-hub' do
  action [:disable, :stop]
end

include_recipe 'selenium_grid::common'

directory node['selenium_grid']['hub']['screencast']['storepath'] do
  only_if { node['selenium_grid']['video_recording']['enabled'] }
  owner SELENINUM_GRID_USER
  group SELENINUM_GRID_USER
end

# Create Selenium grid hub service
template '/usr/lib/systemd/system/selenium-hub.service' do
  source 'systemd/selenium-hub.service.erb'
  mode 0644
  variables(
    :user => SELENINUM_GRID_USER,
    :xmx => node['selenium_grid']['hub']['memory'],
    :screencast_path => node['selenium_grid']['hub']['screencast']['storepath'],
    :jars_path => node['selenium_grid']['server']['installpath'],
    :port => node['selenium_grid']['hub']['port'],
    :options => node['selenium_grid']['hub']['options'],
    :video_enabled => node['selenium_grid']['video_recording']['enabled']
  )
end

# Reload systemd services configurations
execute 'systemctl-daemon-reload' do
  command '/bin/systemctl --system daemon-reload'
  action :nothing
end

# Start the selenium grid node service
service 'selenium-hub' do
  supports :restart => true, :start => true, :stop => true
  action [:enable, :start]
end

if node['selenium_grid']['video_recording']['enabled']
  #### Use nginx to serve screencast directory

  # Install nginx
  yum_package 'nginx'

  # Create Selenium grid hub service
  template '/etc/nginx/conf.d/screeencasts.conf' do
    source 'nginx-screencasts.conf.erb'
    mode 0644
    variables(
      :hub_host => node['selenium_grid']['hub']['host'],
    )
  end

  # Start the nginx
  service 'nginx' do
    supports :restart => true, :start => true, :stop => true
    action [:enable, :start]
  end
end
