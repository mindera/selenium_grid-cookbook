#
# Cookbook Name::selenium_grid
# Recipe::selenium_grid::node
#
# Copyright 2015, Mindera
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

SELENINUM_GRID_USER = node['selenium_grid']['user']
PARALLEL_PROCESSES = node['selenium_grid']['node']['parallel_process']
XVFB_DIR = node['selenium_grid']['xvfb']['dir']
CHROMEDRIVER_LOGFILE = node['selenium_grid']['chromedriver']['logfile']
XVFB_LOGFILE = node['selenium_grid']['xvfb']['logfile']
VIDEO_PORT_OFFSET = 0

(1..PARALLEL_PROCESSES).each do |i|
  # Stop the service if exists
  service "selenium-node_#{i}" do
    action [:disable, :stop]
  end
end

service "selenium-node_video" do
  action [:disable, :stop]
end

include_recipe 'selenium_grid::common'

# Install xorg-x11-server-Xvfb
yum_package 'xorg-x11-server-Xvfb'

# Call specific recipes for instalation and configuration of Chrome, firefox and xvfb
include_recipe 'selenium_grid::chrome'
include_recipe 'selenium_grid::firefox'

(1..PARALLEL_PROCESSES).each do |i|
  directory "#{XVFB_DIR}/screen_#{i}" do
    owner SELENINUM_GRID_USER
    group SELENINUM_GRID_USER
  end
end

directory "#{XVFB_DIR}/screen_#{VIDEO_PORT_OFFSET}" do
  owner SELENINUM_GRID_USER
  group SELENINUM_GRID_USER
  mode '0755'
  only_if { node['selenium_grid']['video_recording']['enabled'] }
end

# Create Selenium grid node configuration
template '/etc/selenium/node.json' do
  source 'node.json.erb'
  mode 0644
  variables(
    :firefoxversion => node['selenium_grid']['firefox']['version'],
    :firefoxmaxinstances => node['selenium_grid']['node']['firefox']['maxInstances'],
    :chromeversion => node['selenium_grid']['chrome']['version'],
    :chromemaxinstances => node['selenium_grid']['node']['chrome']['maxInstances'],
    :firefoxversion => node['selenium_grid']['firefox']['version'],
    :firefoxmaxinstances => node['selenium_grid']['node']['firefox']['maxInstances'],
    :maxSession => node['selenium_grid']['node']['maxSession'],
    :chromeDriverVersion => node['selenium_grid']['chromedriver']['version'],
    :seleniumServerVersion => node['selenium_grid']['server']['version'],
    :applicationName => node['selenium_grid']['video_recording']['enabled'] ? '<video_recording' : 'general'
  )
end

(1..PARALLEL_PROCESSES).each do |i|
  # Create Selenium grid node service
  template "/usr/lib/systemd/system/selenium-node_#{i}.service" do
    source 'systemd/selenium-node.service.erb'
    mode 0644
    owner SELENINUM_GRID_USER
    group SELENINUM_GRID_USER
    variables(
      :user => SELENINUM_GRID_USER,
      :fbsize => node['selenium_grid']['xvfb']['fbsize'],
      :screen_number => (node['selenium_grid']['xvfb']['screen_number'].to_i + i).to_s,
      :xvfb_dir => XVFB_DIR,
      :xvfb_logfile => XVFB_LOGFILE,
      :xmx => node['selenium_grid']['node']['memory'],
      :chromedriver => File.join(node['selenium_grid']['chromedriver']['installpath'], 'chromedriver'),
      :chromedriver_logfile => CHROMEDRIVER_LOGFILE,
      :config => File.join(node['selenium_grid']['server']['confpath'], 'node.json'),
      :jars_path => node['selenium_grid']['server']['installpath'],
      :hub_host => node['selenium_grid']['hub']['host'],
      :hub_port => node['selenium_grid']['hub']['port'],
      :node_port => (node['selenium_grid']['node']['port'].to_i + i).to_s,
      :video_enabled => false,
      :override_firefox_bin => node['selenium_grid']['node']['firefox']['override_bin']
    )
  end
end

template "/usr/lib/systemd/system/selenium-node_video.service" do
  source 'systemd/selenium-node.service.erb'
  mode 0644
  owner SELENINUM_GRID_USER
  group SELENINUM_GRID_USER
  variables(
    :user => SELENINUM_GRID_USER,
    :fbsize => node['selenium_grid']['xvfb']['fbsize'],
    :screen_number => (node['selenium_grid']['xvfb']['screen_number'].to_i + VIDEO_PORT_OFFSET).to_s,
    :xvfb_dir => XVFB_DIR,
    :xvfb_logfile => XVFB_LOGFILE,
    :xmx => node['selenium_grid']['node']['memory'],
    :chromedriver => File.join(node['selenium_grid']['chromedriver']['installpath'], 'chromedriver'),
    :chromedriver_logfile => CHROMEDRIVER_LOGFILE,
    :config => File.join(node['selenium_grid']['server']['confpath'], 'node.json'),
    :jars_path => node['selenium_grid']['server']['installpath'],
    :hub_host => node['selenium_grid']['hub']['host'],
    :hub_port => node['selenium_grid']['hub']['port'],
    :node_port => (node['selenium_grid']['node']['port'].to_i + VIDEO_PORT_OFFSET).to_s,
    :video_enabled => true
  )
  only_if { node['selenium_grid']['video_recording']['enabled'] }
end


# Reload systemd services configurations
execute 'systemctl-daemon-reload' do
  command '/bin/systemctl --system daemon-reload'
  action :nothing
end

(1..PARALLEL_PROCESSES).each do |i|
  # Start the selenium grid node service
  service "selenium-node_#{i}" do
    supports :restart => true, :start => true, :stop => true
    action [:enable, :restart]
  end
end

service "selenium-node_video" do
  action [:enable, :start]
  only_if { node['selenium_grid']['video_recording']['enabled'] }
end

# Configure logrotate
include_recipe 'selenium_grid::logrotate'
