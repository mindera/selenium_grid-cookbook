#
# Cookbook Name:: selenium_grid
# Recipe:: selenium_grid::common
#
# Copyright 2015, Mindera
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

SELENINUM_GRID_USER = node['selenium_grid']['user']
SELENIUM_GRID_INSTALLPATH = node['selenium_grid']['server']['installpath']

# Clean out YUM
execute 'Clean yum metadata' do
  command 'yum clean metadata'
end

# Java Installation for selenium server.
include_recipe 'java::set_attributes_from_version'
include_recipe 'java::oracle'

user SELENINUM_GRID_USER do
  home "/home/#{SELENINUM_GRID_USER}"
  supports :manage_home => true
  action :create
end

[
  SELENIUM_GRID_INSTALLPATH,
  node['selenium_grid']['server']['runpath'],
  node['selenium_grid']['server']['confpath']
].each do |dir_name|
  directory dir_name do
    owner SELENINUM_GRID_USER
    group SELENINUM_GRID_USER
    mode '0755'
  end
end

# Install selenium server standalone
remote_file File.join(SELENIUM_GRID_INSTALLPATH, 'selenium-server-standalone.jar') do
  source 'http://selenium-release.storage.googleapis.com/' \
         "#{node['selenium_grid']['server']['version']}/" \
         "selenium-server-standalone-#{node['selenium_grid']['server']['version']}" \
         ".#{node['selenium_grid']['server']['minor_version']}.jar"
  action :create
  mode 0644
end

# Install selenium video node
remote_file File.join(SELENIUM_GRID_INSTALLPATH, 'selenium-video-node.jar') do
  source node['selenium_grid']['video_recording']['download_url']
  owner SELENINUM_GRID_USER
  group SELENINUM_GRID_USER
  action :create
  mode 0644
  only_if { node['selenium_grid']['video_recording']['enabled'] }
end

# Add necessary repository to get haveged
yum_repository 'epel' do
  description 'Extra Packages for Enterprise Linux 7 - $basearch'
  mirrorlist 'https://mirrors.fedoraproject.org/metalink?repo=epel-7&arch=$basearch'
  baseurl 'http://dl.google.com/linux/chrome/rpm/stable/$basearch'
  gpgkey 'https://dl.fedoraproject.org/pub/epel/RPM-GPG-KEY-EPEL-7'
  failovermethod 'priority'
  gpgcheck true
  action :create
end

# install haveged
yum_package 'haveged'

# Disable epel repository
yum_repository 'epel' do
  enabled false
  action :create
end

# start haveged
service 'haveged' do
  supports :restart => true, :start => true, :stop => true
  action [:enable, :start]
end
