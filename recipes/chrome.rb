#
# Cookbook Name:: selenium_grid
# Recipe:: selenium_grid::chrome
#
# Copyright 2015, Mindera
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

SELENINUM_GRID_USER = node['selenium_grid']['user']
CHROME_DRIVER_VERSION = node['selenium_grid']['chromedriver']['version']
CHROME_DRIVER_INSTALLPATH = node['selenium_grid']['chromedriver']['installpath']

# Add necessary repository to get google chrome
yum_repository 'google-chrome' do
  description 'Repository for google chrome.'
  baseurl 'http://dl.google.com/linux/chrome/rpm/stable/$basearch'
  gpgkey 'https://dl-ssl.google.com/linux/linux_signing_key.pub'
  gpgcheck true
  action :create
end

# Google Chrome
yum_package 'google-chrome-stable' do
  version node['selenium_grid']['chrome']['version']
end

yum_package 'unzip'

# Download Chrome driver
remote_file "#{Chef::Config[:file_cache_path]}/" \
            "chromedriver_linux64_#{CHROME_DRIVER_VERSION}.zip" do
  source "http://chromedriver.storage.googleapis.com/#{CHROME_DRIVER_VERSION}" \
         '/chromedriver_linux64.zip'
  action :create
  notifies :run, 'execute[unpack chromedriver]', :immediately
end

directory CHROME_DRIVER_INSTALLPATH do
  owner SELENINUM_GRID_USER
  group SELENINUM_GRID_USER
end

# Unpack Chrome driver
execute 'unpack chromedriver' do
  command "unzip -o #{Chef::Config[:file_cache_path]}/" \
          "chromedriver_linux64_#{node['selenium_grid']['chromedriver']['version']}.zip" \
          " -d #{CHROME_DRIVER_INSTALLPATH}"
  action :nothing
end

# Change permissions to Chrome driver
file File.join(CHROME_DRIVER_INSTALLPATH, 'chromedriver') do
  mode 0755
  owner SELENINUM_GRID_USER
  group SELENINUM_GRID_USER
end
