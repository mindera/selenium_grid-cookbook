#
# Cookbook Name:: selenium_grid
# Attributes:: video
#
# Copyright 2015, Mindera
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

default['selenium_grid']['video_recording']['download_url'] = \
    'http://repo1.maven.org/maven2/com/aimmac23/selenium-video-node/1.8/selenium-video-node-1.8.jar'
