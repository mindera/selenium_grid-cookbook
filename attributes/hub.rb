#
# Cookbook Name:: selenium_grid
# Attributes:: hub
#
# Copyright 2015, Mindera
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

selenium_hub = default['selenium_grid']['hub']
selenium_hub['host'] = nil
selenium_hub['port'] = 4444
selenium_hub['memory'] = '512m'
selenium_hub['options'] = '-timeout 60 -browserTimeout 120'
selenium_hub['screencast']['storepath'] = '/opt/selenium/screencast'
