#
# Cookbook Name:: selenium_grid
# Attributes:: node
#
# Copyright 2015, Mindera
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

default['selenium_grid']['node']['port'] = '5555'
default['selenium_grid']['node']['memory'] = '512m'
default['selenium_grid']['node']['firefox']['maxInstances'] = 1

default['selenium_grid']['node']['chrome']['maxInstances'] = 1
default['selenium_grid']['node']['maxSession'] = 1
default['selenium_grid']['node']['parallel_process'] = 5

default['selenium_grid']['chrome']['version'] = '51.0.2704.106-1'
default['selenium_grid']['chromedriver']['version'] = '2.21'
default['selenium_grid']['chromedriver']['installpath'] = '/opt/selenium/drivers'
default['selenium_grid']['chromedriver']['logfile'] = '/var/log/chromedriver.log'

default['selenium_grid']['firefox']['version'] = '38.6.0-1.el7.centos'
default['selenium_grid']['firefox']['from_binary'] = true

default['selenium_grid']['xvfb']['fbsize'] = '1280x1024x24'
default['selenium_grid']['xvfb']['screen_number'] = 0
default['selenium_grid']['xvfb']['dir'] = '/opt/selenium/xvfb'
default['selenium_grid']['xvfb']['logfile'] = '/var/log/xvfb.log'

default['java']['oracle']['accept_oracle_download_terms'] = true
