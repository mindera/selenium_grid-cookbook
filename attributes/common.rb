#
# Cookbook Name:: selenium_grid
# Attributes:: common
#
# Copyright 2015, Mindera
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

selenium_grid = default['selenium_grid']
selenium_grid['user'] = 'selenium'

server = default['selenium_grid']['server']

server['version'] = '2.47'
server['minor_version'] = '0'
server['installpath'] = '/opt/selenium'
server['runpath'] = '/run/selenium'
server['confpath'] = '/etc/selenium'

# Java
default['java']['jdk_version'] = 8
