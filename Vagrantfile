# -*- mode: ruby -*-
# vim: set ft=ruby :

boxes = {
  :hub => {
    :ipaddress => '192.168.56.10',
    :cpus => 1
  },
  :node => {
    :ipaddress => '192.168.56.11',
    :cpus => 3
  }
}

VAGRANTFILE_API_VERSION = '2'

Vagrant.configure(VAGRANTFILE_API_VERSION) do |global_config|
  boxes.each_pair do |name, options|
    global_config.vm.define name do |config|
      if Vagrant.has_plugin?('vagrant-omnibus')
        config.omnibus.chef_version = :latest
      else
        error = 'The vagrant-omnibus plugin is not installed! Try running:\nvagrant plugin install vagrant-omnibus'
        fail Exception, error
      end

      if Vagrant.has_plugin?('vagrant-berkshelf')
        config.berkshelf.enabled = true
      else
        error = 'The vagrant-berkshelf plugin is missing - Try: vagrant plugin install vagrant-berkshelf'
        fail Exception, error
      end

      ipaddress = options[:ipaddress]
      config.vm.box = 'bento/centos-7.1'
      config.vm.hostname = name
      config.vm.network 'private_network', ip: ipaddress
      config.vm.synced_folder 'vagrant-root', '/vagrant', disabled: true

      config.vm.provider 'virtualbox' do |vb|
        vb.customize ['modifyvm', :id, '--memory', '2048']
        vb.customize ['modifyvm', :id, '--cpus', options[:cpus]]
      end

      json_hash = {
        'selenium_grid' => {
          'hub' => {
            'host' => boxes[:hub][:ipaddress]
          },
          'node' => {
            'parallel_process' => 3
          }
        }
      }

      # Provision the box
      config.vm.provision 'chef_solo' do |chef|
        chef.add_recipe "selenium_grid::#{name}"
        chef.json = json_hash
      end
    end
  end
end
